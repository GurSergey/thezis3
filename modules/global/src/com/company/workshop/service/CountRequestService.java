/*
 * Copyright (c) 2020 com.company.workshop.service
 */
package com.company.workshop.service;

import com.haulmont.thesis.core.entity.Contractor;

/**
 * @author serge
 */
public interface CountRequestService {
    String NAME = "workshop_CountRequestService";
    public Long getCountForCustomer(Contractor customer);
}