/*
 * Copyright (c) 2020 com.company.workshop.entity
 */
package com.company.workshop.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;

/**
 * @author serge
 */
public enum TypeAuto implements EnumClass<Integer> {

    crossover(1),
    wagon(2),
    sedan(3);

    private Integer id;

    TypeAuto(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public static TypeAuto fromId(Integer id) {
        for (TypeAuto at : TypeAuto.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}