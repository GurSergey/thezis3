/*
 * Copyright (c) 2020 com.company.workshop.entity
 */
package com.company.workshop.entity;

import javax.annotation.PostConstruct;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.PrimaryKeyJoinColumn;

import com.company.workshop.service.CountRequestService;
import com.haulmont.chile.core.annotations.MetaProperty;
import javax.persistence.Transient;

import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.thesis.core.entity.Individual;

/**
 * @author serge
 */
@PrimaryKeyJoinColumn(name = "CORRESPONDENT_ID", referencedColumnName = "CONTRACTOR_ID")
@Table(name = "WORKSHOP_EXT_INDIVIDUAL")
@Entity(name = "workshop$ExtIndividual")
public class ExtIndividual extends Individual {
    private static final long serialVersionUID = 806638450396795402L;

    @Transient
    @MetaProperty
    protected Long countRequest;

    public void setCountRequest(Long countRequest) {
        this.countRequest = countRequest;
    }

    public Long getCountRequest() {
        return countRequest;
    }

    @PostConstruct
    private void initCountry() {
        if (countRequest == null) {
            CountRequestService countryService = AppBeans.get(CountRequestService.class);
            countRequest = countryService.getCountForCustomer(this);
        }
    }

}