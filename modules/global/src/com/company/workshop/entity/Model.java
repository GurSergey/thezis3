/*
 * Copyright (c) 2020 com.company.workshop.entity
 */
package com.company.workshop.entity;


/**
 * @author serge
 */
import com.haulmont.cuba.core.entity.annotation.EnableRestore;
import com.haulmont.cuba.core.entity.annotation.TrackEditScreenHistory;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.haulmont.chile.core.annotations.NamePattern;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.StandardEntity;

@NamePattern("%s|name")
@Table(name = "WORKSHOP_MODEL")
@Entity(name = "workshop$Model")
@EnableRestore
@TrackEditScreenHistory
public class Model extends StandardEntity {
    private static final long serialVersionUID = 9210484859110200909L;

    @Column(name = "NAME", nullable = false, length = 50)
    protected String name;

    @Column(name = "CODE", nullable = false, length = 50)
    protected String code;

    @Column(name = "DESCRIPTION", length = 50)
    protected String description;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


}