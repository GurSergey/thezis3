/*
 * Copyright (c) 2020 com.company.workshop.entity
 */
package com.company.workshop.entity;


/**
 * @author serge
 */
import com.haulmont.cuba.core.entity.annotation.EnableRestore;
import com.haulmont.cuba.core.entity.annotation.TrackEditScreenHistory;
import javax.persistence.Entity;
import javax.persistence.DiscriminatorValue;
import javax.persistence.InheritanceType;
import javax.persistence.Inheritance;
import com.haulmont.cuba.core.entity.annotation.Listeners;

@Listeners("workshop_AutoListener")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("2")
@Entity(name = "workshop$Auto")
@EnableRestore
@TrackEditScreenHistory
public class Auto extends AutoBase {
    private static final long serialVersionUID = -8550554517702370772L;

}