/*
 * Copyright (c) 2020 com.company.workshop.entity
 */
package com.company.workshop.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.DiscriminatorValue;
import javax.persistence.InheritanceType;
import javax.persistence.Inheritance;
import javax.persistence.PrimaryKeyJoinColumn;
import com.haulmont.cuba.core.entity.annotation.SystemLevel;
import com.haulmont.thesis.core.annotation.CardBase;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.haulmont.thesis.core.entity.Employee;
import com.haulmont.thesis.core.entity.TsCard;

/**
 * @author serge
 */
@CardBase
@SystemLevel
@PrimaryKeyJoinColumn(name = "CARD_ID", referencedColumnName = "ID")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue("1")
@Table(name = "WORKSHOP_AUTO_BASE")
@Entity(name = "workshop$AutoBase")
public class AutoBase extends TsCard {
    private static final long serialVersionUID = -8738497925107129283L;

    @Column(name = "NUMBER_", length = 50)
    protected String number;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MODEL_ID")
    protected Model model;

    @Column(name = "YEAR_", length = 50)
    protected Integer year;

    @Column(name = "COST", length = 50)
    protected Integer cost;

    @Column(name = "TYPE_")
    protected Integer type;

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Model getModel() {
        return model;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getYear() {
        return year;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Integer getCost() {
        return cost;
    }

    public TypeAuto getType() {
        return type == null ? null : TypeAuto.fromId(type);
    }

    public void setType(TypeAuto type) {

        this.type = type == null ? null : type.getId();
    }

}