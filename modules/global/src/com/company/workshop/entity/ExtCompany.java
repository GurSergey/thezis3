/*
 * Copyright (c) 2020 com.company.workshop.entity
 */
package com.company.workshop.entity;

import javax.annotation.PostConstruct;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Column;

import com.company.workshop.service.CountRequestService;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.thesis.core.entity.Company;
import com.haulmont.chile.core.annotations.MetaProperty;
import javax.persistence.Transient;

/**
 * @author serge
 */
@PrimaryKeyJoinColumn(name = "CORRESPONDENT_ID", referencedColumnName = "CONTRACTOR_ID")
@Table(name = "WORKSHOP_EXT_COMPANY")
@Entity(name = "workshop$ExtCompany")
public class ExtCompany extends Company {
    private static final long serialVersionUID = -8348522022921538677L;

    @Transient
    @MetaProperty
    protected Long countRequest;

    public void setCountRequest(Long countRequest) {
        this.countRequest = countRequest;
    }

    public Long getCountRequest() {
        return countRequest;
    }


    @PostConstruct
    private void initCountry() {
        if (countRequest == null) {
            CountRequestService countryService = AppBeans.get(CountRequestService.class);
            countRequest = countryService.getCountForCustomer(this);
        }
    }

}