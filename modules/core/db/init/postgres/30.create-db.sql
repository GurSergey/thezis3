INSERT INTO sec_role (ID, CREATE_TS, CREATED_BY, VERSION, NAME, LOC_NAME,DESCRIPTION) VALUES (newid(),
CURRENT_TIMESTAMP, USER, 1, 'Manager', 'Менеджер', '1')^
select create_or_update_sec_permissi('Manager', 'workshop$AutoBase:create', 20, 1)^
select create_or_update_sec_permissi('Manager', 'workshop$AutoBase:update', 20, 1)^
select create_or_update_sec_permissi('Manager', 'workshop$AutoBase:delete', 20, 1)^
select create_or_update_sec_permissi('Manager', 'workshop$PurchaseRequest:create', 20, 1)^
select create_or_update_sec_permissi('Manager', 'workshop$PurchaseRequest:update', 20, 1)^
select create_or_update_sec_permissi('Manager', 'workshop$PurchaseRequest:delete', 20, 1)^

INSERT INTO sec_role (ID, CREATE_TS, CREATED_BY, VERSION, NAME, LOC_NAME,DESCRIPTION) VALUES (newid(),
CURRENT_TIMESTAMP, USER, 1, 'Operator', 'Оператор банка', '1')^
select create_or_update_sec_permissi('Operator', 'workshop$AutoBase:create', 20, 1)^
select create_or_update_sec_permissi('Operator', 'workshop$AutoBase:update', 20, 1)^
select create_or_update_sec_permissi('Operator', 'workshop$AutoBase:delete', 20, 1)^
select create_or_update_sec_permissi('Operator', 'workshop$PurchaseRequest:update', 20, 1)^
select create_or_update_sec_permissi('Operator', 'workshop$PurchaseRequest:delete', 20, 1)^

INSERT INTO sec_role (ID, CREATE_TS, CREATED_BY, VERSION, NAME, LOC_NAME,DESCRIPTION) VALUES (newid(),
CURRENT_TIMESTAMP, USER, 1, 'Master', 'Мастер', '1')^
select create_or_update_sec_permissi('Master', 'workshop$AutoBase:create', 20, 1)^
select create_or_update_sec_permissi('Master', 'workshop$AutoBase:update', 20, 1)^
select create_or_update_sec_permissi('Master', 'workshop$AutoBase:delete', 20, 1)^
select create_or_update_sec_permissi('Master', 'workshop$PurchaseRequest:update', 20, 1)^
select create_or_update_sec_permissi('Master', 'workshop$PurchaseRequest:delete', 20, 1)^
