--Add default doc kind for workshop$PurchaseRequest
CREATE OR REPLACE FUNCTION baseInsert()
RETURNS integer
AS $$
DECLARE
cnt integer = 0;
BEGIN
cnt = (select count(CATEGORY_ID) from DF_DOC_KIND where category_id = '3750d45b-176e-4f48-bd14-5da8865c438f');
if(cnt = 0) then
    insert into SYS_CATEGORY (ID, NAME, ENTITY_TYPE, IS_DEFAULT, CREATE_TS, CREATED_BY, VERSION, DISCRIMINATOR)
    values ( '3750d45b-176e-4f48-bd14-5da8865c438f', 'Заявка на покупку автомобиля', 'workshop$PurchaseRequest', false, now(), USER, 1, 1);

    insert into DF_DOC_KIND (category_id, create_ts, created_by, version, doc_type_id, numerator_id, 
    numerator_type, category_attrs_place, tab_name, portal_publish_allowed, disable_add_process_actors, create_only_by_template)
    values ('3750d45b-176e-4f48-bd14-5da8865c438f', 'now()', 'admin', 1, 'a8e4d753-d4c1-4edd-9adf-39a19a5f59a2', 'eaa9b83d-4da6-4812-8dd8-20ba22b1f976', 
    1, 1, 'Р”РѕРї. РїРѕР»СЏ', false, false, false);
end if;return 0;

END;
$$
LANGUAGE plpgsql;
^
select baseInsert();
^
drop function if exists baseInsert()^
