--Update process card_types for entity workshop$PurchaseRequest
update wf_proc set card_types = regexp_replace(card_types, E',workshop\\$PurchaseRequest', '') where code in ('Endorsement','Resolution','Acquaintance','Registration')^
update wf_proc set updated_by='admin', card_types = card_types || 'workshop$PurchaseRequest,' where code in ('Endorsement','Resolution','Acquaintance','Registration')^
