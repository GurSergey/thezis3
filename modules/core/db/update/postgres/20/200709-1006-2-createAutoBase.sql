alter table WORKSHOP_AUTO_BASE add constraint FK_WORKSHOP_AUTO_BASE_MODEL_ID foreign key (MODEL_ID) references WORKSHOP_MODEL(ID);
alter table WORKSHOP_AUTO_BASE add constraint FK_WORKSHOP_AUTO_BASE_CARD_ID foreign key (CARD_ID) references WF_CARD(ID);
create index IDX_WORKSHOP_AUTO_BASE_MODEL on WORKSHOP_AUTO_BASE (MODEL_ID);
