/*
 * Copyright (c) 2020 com.company.workshop.service
 */
package com.company.workshop.service;

import com.haulmont.cuba.core.*;
import com.haulmont.thesis.core.entity.Company;
import com.haulmont.thesis.core.entity.Contractor;
import com.haulmont.thesis.core.entity.Individual;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

/**
 * @author serge
 */
@Service(CountRequestService.NAME)
public class CountRequestServiceBean implements CountRequestService {

    @Inject
    private Persistence persistence;

    @Override
    public Long getCountForCustomer(Contractor customer) {
        Transaction tx = persistence.createTransaction();
        EntityManager em = persistence.getEntityManager();
        Query query =
                em.createQuery("SELECT COUNT(r) FROM workshop$PurchaseRequest r WHERE r.customer = :customer ",
                        Long.class);
        Long res = (Long) query.setParameter("customer", customer).getFirstResult();
        tx.commit();
        return res;
    }
}