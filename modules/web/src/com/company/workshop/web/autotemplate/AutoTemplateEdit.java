
package com.company.workshop.web.autotemplate;

import com.company.workshop.entity.AutoTemplate;
import com.company.workshop.web.auto.AutoEdit;

public class AutoTemplateEdit extends AutoEdit<AutoTemplate> {

    @Override
    protected boolean isNumberAssignNeeded() {
        return false;
    }

    @Override
    protected boolean isImportantButtonVisible() {
        return false;
    }
}